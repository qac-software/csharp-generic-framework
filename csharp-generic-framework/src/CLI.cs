﻿using NDesk.Options;
using System.Collections.Generic;

namespace csharp_generic_framework
{
    public class CLI
    {
        //These two variables are here for examples purposes. 
        //Remove or rename them when appropriate. 
        bool   exampleHelpBool   = false;
        string configFileString  = "";

        /// <summary>
        /// Constructor
        /// </summary>
        public CLI()
        {

        }

        /// <summary>
        /// Parse the arguments given by the main entry point 
        /// </summary>
        /// <param name="args">Application arguments</param>
        public void ParseArgs(string[] args)
        {
            OptionSet    _set   = GetOptions();
            List<string> _extra = _set.Parse(args);

            if (exampleHelpBool)
            {
                _set.WriteOptionDescriptions(System.Console.Out);
                return; 
            }
    
        }

        /// <summary>
        /// Return our set of expected Options as an OptionSet
        /// 
        /// Each Option is defined as the following: 
        /// - Argument 1: Pipe delimited flags
        /// - Argument 2: Description, Optional.
        /// - Argument 3: Delegate method accepting a string 
        /// 
        ///   Note: The string the delegate receives is based on the flag given.
        ///   If the flag contains an assignment operator, the right hand of the assignment is passed.
        ///   Otherwise the flag is passed. 
        ///   
        ///   More information: http://www.ndesk.org/Options
        /// </summary>
        /// <returns></returns>
        private OptionSet GetOptions()
        {
            return new OptionSet()
            {
                //When no assignment operator is found in the flag ('='), the value of the flag itself is passed to the delegate
                { "h|help", "Show this message and exit.", v => exampleHelpBool = v != null },
                //When an assignment operator is found, the value on the right hand of the assignment is passed to the delegate
                { "config=",  "Set the configuration file",  v => configFileString = v }
                
            };
        }

        public string GetConfigFile()
        {
            return configFileString;
        }
    }
}
