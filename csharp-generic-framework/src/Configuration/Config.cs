﻿using System;
using OpenQA.Selenium.Remote;

/// <summary>
/// Centralized location of all config information.
///
/// Appending a new TOML Section:
///  - Create a new public class representing a new section in the TOML 
///  - Create a public variable for each configuration option with public accessors
///  - Add an instance of this new class to Config with public accessors
///
///  Note: Sections + Fields MUST be capitalized, otherwise it will not pull the information 
///
///  For example:
///
///  Example.toml
///  ------------
///  [MySection]
///  Test="test"
///
///
///  Config.java
///  -----------
///  public class MySection extends Section
///  {
///      public string Test {get;set;}
///  }
///  public class Config
///  {
///      public MySection MySection {get;set;}
///      ...
///  }
///
///  With this setup, the TOML parser will automatically assign the variables with their appropriate values
///  from the external configuration file.
/// </summary>
namespace csharp_generic_framework.Configuration
{
    //Contains all data under the [Project] header in the TOML file 
    public class Project
    {
        public string Name { get; set; }
    }

    //Contains all data under the [Pathing] header in the TOML file 
    public class Pathing
    {
        public string WebDrivers { get; set; }
        public string Logs { get; set; }
        public string Reports { get; set; }
    }

    //Contains all the data under the [Capabilities] header in the TOML file 
    public class Capabilities
    {
        public string URL { get; set; }
        public string BrowserName { get; set; }
        public string BrowserVersion { get; set; }
        public string Platform { get; set; }

        public DesiredCapabilities GetCapabilities()
        {
            DesiredCapabilities _return = new DesiredCapabilities();
            _return.SetCapability("browserName", BrowserName);
            _return.SetCapability("browserVersion", BrowserVersion);
            _return.SetCapability("platform", Platform);
            return _return;
        }
    }



    public class Config
    {
        public Project Project { get; set; }
        public Pathing Pathing { get; set; }
        public Capabilities Capabilities { get; set; }
 
        private static Config mInstance; 

        /// <summary>
        /// Constructor 
        /// </summary>
        public Config()
        {}

        /// <summary>
        /// Returns the internal instance of Config 
        /// </summary>
        /// <returns>Internal instance of Config</returns>
        public static Config GetInstance()
        {
            if (mInstance == null)
                throw new Exception("Failed to return instance of Config. Please call Load() with a parser object");
            return mInstance;
        }

        /// <summary>
        /// Loads the internal instance of Config using a concrete IConfigParser object
        /// </summary>
        /// <param name="_parser">IConfigParser object</param>
        public static void Load(IConfigParser<Config> _parser)
        {
            mInstance = _parser.Parse();
        }
    }
}
