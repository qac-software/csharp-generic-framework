﻿namespace csharp_generic_framework.Configuration
{
    /// <summary>
    /// IConfigParser is the interface for all of our configuration parsers.
    /// </summary>
    /// <typeparam name="T">The return type of this particular parser. Allows it to be used for other purposes in the future</typeparam>
    public interface IConfigParser<T> 
    {
        T Parse();
    }
}
