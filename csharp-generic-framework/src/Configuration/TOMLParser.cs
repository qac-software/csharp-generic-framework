﻿using System;
using Nett;

namespace csharp_generic_framework.Configuration
{
    /// <summary>
    /// TOMLParser is a concrete implementation of IConfigParser that parsers and maps
    /// a TOML file to a C# class. 
    /// 
    /// This requires that the sections of a TOML file be represented as individual classes in the Configuration class,
    /// with each field of the class mapping to a key/value pair in the TOML. 
    /// 
    /// See: Config.cs for more information
    /// </summary>
    class TOMLParser : IConfigParser<Config>
    {
        // Internal reference to the TOML file 
        private String mFilePath;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_filePath">Path to the TOML file</param>
        public TOMLParser(String _filePath)
        {
            mFilePath = _filePath;
        }

        public Config Parse()
        {
            return Toml.ReadFile<Config>(mFilePath);
        }
    }
}
