﻿using System;
using OpenQA.Selenium;
using csharp_generic_framework.src.Keywords;

namespace csharp_generic_framework.Keywords.Interaction 
{
    /// <summary>
    /// Concrete implementation of Keyword responsible for clicking on the given IWebElement using
    /// Selenium's default click functionality
    /// </summary>
    public class Click : StandardKeyword<IWebElement>
    {
        public override bool Execute(IWebElement _element, string[] arguments)
        {
            try
            {
                _element.Click();
                return true;
            }
            catch (Exception e)
            {
                mLogger.Error("An error has occurred in the Click keywords", e);
                throw e;
            }
        }
    }
}
