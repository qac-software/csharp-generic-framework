﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using csharp_generic_framework.src.Keywords;

namespace csharp_generic_framework.Keywords.Interaction
{
    /// <summary>
    /// Concrete implementation of Keyword responsible for selecting a dropdown option on the 
    /// given IWebElement using Selenium's SelectElement object 
    /// </summary>
    public class Dropdown : StandardKeyword<IWebElement>
    {
        public enum SelectBy
        {
            INDEX, VALUE, VISIBLE_TEXT, UNSPECIFIED
        }

        // Selenium SelectElement object
        private SelectElement mSelect;
        // Selection Strategy 
        private SelectBy      mStrategy;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_strategy">Dropdown selection strategy</param>
        public Dropdown(SelectBy _strategy) { mStrategy = _strategy; }

        public override bool Execute(IWebElement _element, string[] _arguments)
        {
            try
            {
                mSelect = new SelectElement(_element);
                switch (mStrategy)
                {
                    case SelectBy.INDEX:
                        SelectByIndex(mSelect, int.Parse(_arguments[0]));
                        break;
                    case SelectBy.VALUE:
                        SelectByValue(mSelect, _arguments[0]);
                        break;
                    case SelectBy.VISIBLE_TEXT:
                        SelectByText(mSelect, _arguments[0]);
                        break;
                    default:
                        return false;
                }
                return true;
            }
            catch (Exception e)
            {
                mLogger.Error("An error has occurred in the Dropdown keyword", e);
                throw e;
            }
        }

        /// <summary>
        /// Selects from the drop-down object using an index value 
        /// </summary>
        /// <param name="_select">Dropdown as a SelectElement object</param>
        /// <param name="_index">Index</param>
        private void SelectByIndex(SelectElement _select, int _index)
        {
            _select.SelectByIndex(_index);
        }

        /// <summary>
        /// Selects from the drop-down object using the value 
        /// </summary>
        /// <param name="_select">Dropdown as a SelectElement object</param>
        /// <param name="_value">Value</param>
        private void SelectByValue(SelectElement _select, string _value)
        {
            _select.SelectByValue(_value);
        }

        /// <summary>
        /// Selects from the drop-down object using the text 
        /// </summary>
        /// <param name="_select">Dropdown as a SelectElement object</param>
        /// <param name="_text">Text</param>
        private void SelectByText(SelectElement _select, string _text)
        {
            _select.SelectByText(_text);
        }


    }
}
