﻿using System;
using OpenQA.Selenium;
using csharp_generic_framework.src.Keywords;

namespace csharp_generic_framework.Keywords.Interaction
{
    /// <summary>
    /// Concrete implementation of Keyword responsible for inputting text into hte given IWebElement
    /// using Selenium's default sendKeys functionality 
    /// </summary>
    public class Input : StandardKeyword<IWebElement>
    {
        private bool mAppend;

        public Input() { }

        public Input(bool _append)
        {
            mAppend = _append;
        }

        public override bool Execute(IWebElement _element, string[] _arguments)
        {
            try
            {
                if (!mAppend)
                    _element.Clear();

                if(_arguments[0] != null)
                {
                    _element.SendKeys(_arguments[0]);
                    return true;
                }
                mLogger.Warn("No text supplied to input - could not input text");
                return false;
            }
            catch(Exception e)
            {
                mLogger.Error("An error has occurred in the Input keyword", e);
                throw e;
            }
        }
    }
}
