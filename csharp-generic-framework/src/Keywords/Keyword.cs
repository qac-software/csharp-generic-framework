﻿using log4net;

namespace csharp_generic_framework.Keywords
{
    /// <summary>
    ///  Keyword is an abstract class used as a base for all Keywords.
    /// 
    ///  The intention behind wrapping Selenium functionality in keywords is for future integration with a keyword
    ///  driven environment. A client may require tests be run through an external data format, eg: Excel XLSX files.
    /// 
    ///  Using this abstraction, we can easily call the functionality that we need in a single line without having to
    ///  rewrite boilerplate functionality for the new input method.
    /// 
    ///  It is recommended that additional reusable Selenium functionality be implemented in Keywords for this purpose.
    /// 
    ///  Note:
    ///   Templates are used to address the differences in arguments that Keywords may require.
    ///   Typically this only changes between WebElement and WebDriver
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="R"></typeparam>
    public abstract class Keyword<T, R>
    {
        protected static ILog mLogger = LogManager.GetLogger(typeof(Keyword<T, R>));

        /// <summary>
        /// Execute the Keyword
        /// </summary>
        /// <param name="_generic">Generic object to use by the keywords</param>
        /// <param name="arguments">Arguments to be used by this Keyword</param>
        /// <returns></returns>
        public abstract bool Execute(T _generic, R _arguments);
    }
}
