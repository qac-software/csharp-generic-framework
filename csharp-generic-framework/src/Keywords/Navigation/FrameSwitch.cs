﻿using System;
using OpenQA.Selenium;
using csharp_generic_framework.src.Keywords;

namespace csharp_generic_framework.Keywords.Navigation
{
    /// <summary>
    /// Concrete implementation of Keyword responsible for switching IFrames on the current page.
    /// 
    /// If the given argument is an integer, it will be parsed and frames will be switched based on the index.
    /// If the given argument is a string, it will be used to locate and switch based on name/id.
    /// </summary>
    public class FrameSwitch : StandardKeyword<IWebDriver>
    {
        public override bool Execute(IWebDriver _driver, string[] _arguments)
        {
            try
            {
                int _index;
                if (int.TryParse(_arguments[0], out _index))
                    _driver.SwitchTo().Frame(_index);                              // Switch using the index value
                else
                    _driver.SwitchTo().Frame(_arguments[0]);                       // Switch using the frame name
                return true;
            }
            catch (Exception e)
            {
                mLogger.Error("An error has occurred in the FrameSwitch keyword", e);
                throw e;
            }
        }
    }
}
