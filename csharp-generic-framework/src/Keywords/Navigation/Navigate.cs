﻿using System;
using OpenQA.Selenium;
using csharp_generic_framework.src.Keywords;

namespace csharp_generic_framework.Keywords.Navigation
{
    /// <summary>
    /// Concrete implementation of Keyword responsible forn avigating to a URL,
    /// forwards, and backwards.
    /// </summary>
    public class Navigate : StandardKeyword<IWebDriver>
    {
        public enum Destination
        {
            URL, BACK, FORWARD
        }

        // Navigation Strategy
        private Destination mDestination;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_destination">Enum value determining how to navigate</param>
        public Navigate(Destination _destination)
        {
            mDestination = _destination;
        }

        public override bool Execute(IWebDriver _driver, string[] _arguments)
        {
            try
            {
                switch (mDestination)
                {
                    case Destination.URL:
                        NavigateToURL(_driver, _arguments[0]);
                        break;
                    case Destination.BACK:
                        NavigateBack(_driver);
                        break;
                    case Destination.FORWARD:
                        NavigateForward(_driver);
                        break;
                    default:
                        return false;
                }
                return true;
            }
            catch (Exception e)
            {
                mLogger.Error("An error has occurred in the Navigate keyword", e);
                throw e;
            }
        }

        /// <summary>
        /// Navigate to the supplied URL 
        /// Performs a basic URL validity check prior to navigation
        /// </summary>
        /// <param name="_driver">WebDriver instance</param>
        /// <param name="_url">URL to navigate to</param>
        private void NavigateToURL(IWebDriver _driver, String _url)
        {
            if (_url.StartsWith("http") || _url.StartsWith("file"))
                _driver.Navigate().GoToUrl(_url);
            else
                throw new Exception("Invalid URL");
        }

        /// <summary>
        /// Navigate to the previous page, if possible
        /// </summary>
        /// <param name="_driver">WebDriver instance</param>
        private void NavigateBack(IWebDriver _driver)
        {
            _driver.Navigate().Back();
        }

        /// <summary>
        /// Navigate to the next page, if possible.
        /// This function should only be used if you've previously navigated backwards
        /// </summary>
        /// <param name="_driver">WebDriver instance</param>
        private void NavigateForward(IWebDriver _driver)
        {
            _driver.Navigate().Forward();
        }
    }
}
