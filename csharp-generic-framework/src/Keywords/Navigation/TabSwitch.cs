﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using csharp_generic_framework.src.Keywords;

namespace csharp_generic_framework.Keywords.Navigation
{
    /// <summary>
    /// Concrete implementation of Keyword responsible for switching tabs in the current window.
    /// 
    /// If the given argument is an integer, it will be parsed and tabs will be switched based on the index.
    /// If the given argument is a string, it will be used to switch based on the title.
    /// </summary>
    public class TabSwitch : StandardKeyword<IWebDriver>
    {
        public override bool Execute(IWebDriver _driver, string[] _arguments)
        {
            try
            {
                int _index;
                List<String> _tabs = new List<String>(_driver.WindowHandles);
                if (int.TryParse(_arguments[0], out _index))
                    _driver.SwitchTo().Window(_tabs[_index - 1]);
                else
                    _driver.SwitchTo().Window(_arguments[0]);
                return true;
            }
            catch (Exception e)
            {
                mLogger.Error("An error has occurred in the TabSwitch keyword", e);
                throw e;
            }
        }
    }
}
