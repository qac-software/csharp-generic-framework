﻿using System;
using OpenQA.Selenium;

namespace csharp_generic_framework.Keywords.Validation
{
    /// <summary>
    /// Concrete implementation of Keyword responsible for validating various aspects of a WebElement
    /// </summary>
    public class ValidateElement : Keyword<Predicate<IWebElement>, IWebElement>
    {
        public override bool Execute(Predicate<IWebElement> _strategy, IWebElement _element)
        {
            try
            {
                return _strategy.Invoke(_element);
            }
            catch (Exception e)
            {
                mLogger.Error("An error has occurred in the ValidateElement keyword", e);
                throw e;
            }
        }
        //Validation Methods
        public static Predicate<IWebElement> GetTextEqualsStrategy(String _expected)
        {
            return (IWebElement e) => e.Text.Equals(_expected);
        }

        public static Predicate<IWebElement> GetTextContainsStrategy(String _expected)
        {
            return (IWebElement e) => e.Text.Contains(_expected);
        }

        public static Predicate<IWebElement> GetAttributeEqualsStrategy(String _attr, String _expected)
        {
            return (IWebElement e) => e.GetAttribute(_attr).Equals(_expected);
        }

        public static Predicate<IWebElement> GetElementEnabledStrategy(bool _positive)
        {
            return (IWebElement e) => e.Enabled == _positive;
        }

        public static Predicate<IWebElement> GetElementVisibleStrategy(bool _positive)
        {
            return (IWebElement e) => e.Displayed == _positive;
        }
    }
}
