﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using csharp_generic_framework.src.Keywords;
using System.Diagnostics;

namespace csharp_generic_framework.Keywords
{
    /// <summary>
    /// Concrete implementation of Keyword responsible for waiting until an element
    /// is one of the following: 
    /// 
    /// Enabled, Selected, Visible 
    /// </summary>
    public class Wait : StandardKeyword<Func<IWebDriver, Boolean>>
    {
        protected WebDriverWait mWebDriverWait;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_driver">IWebDriver instance</param>
        /// <param name="_for">enum value determining wait type</param>
        public Wait(IWebDriver _driver)
        {
            mWebDriverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
        }

        public override bool Execute(Func<IWebDriver, bool> _func, string[] _arguments)
        {
            try
            {
                mWebDriverWait.Until(_func);
                return true;
            }
            catch (Exception e)
            {
                mLogger.Error("An error has occurred in the Wait keyword", e);
                throw e;
            }
        }

        //Wait Methods
        public static Func<IWebDriver, bool> waitForEnabledStrategy(IWebElement _element)
        {
            return (IWebDriver d) => _element.Enabled;
        }

        public static Func<IWebDriver, bool> waitForSelectedStrategy(IWebElement _element)
        {
            return (IWebDriver d) => _element.Selected;
        }

        public static Func<IWebDriver, bool> waitForVisibleStrategy(IWebElement _element)
        {
            return (IWebDriver d) => _element.Displayed;
        }

        public static Func<IWebDriver, bool> waitForTimeStrategy(int _time)
        {
            double _currentTime = getTimeMillis();
            return (IWebDriver d) => getTimeMillis() > (_currentTime + (_time * 1000));
        }

        private static double getTimeMillis()
        {
            return (DateTime.Now - DateTime.MinValue).TotalMilliseconds;
        }
    }
}
