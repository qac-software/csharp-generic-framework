﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

/// <summary>
/// DriverFactory is a helper class created to facilitate the creation of WebDrivers
/// </summary>
namespace csharp_generic_framework.Misc
{
    public static class DriverFactory
    {
        public enum Browser
        {
            FIREFOX, CHROME, IE
        }

        private static String sWebDriverRoot;

        /// <summary>
        /// Sets the root directory of the DriverFactory to be used when determining the location of a 
        /// WebDriver binary
        /// </summary>
        /// <param name="_dir">Directory path</param>
        public static void SetWebDriverRoot(string _dir)
        {
            sWebDriverRoot = _dir;
        }

        /// <summary>
        /// Helper method to get a local driver based only on the Browser enumeration w/o capabilities
        /// </summary>
        /// <param name="_type">Browser enum</param>
        /// <returns>WebDriver instance</returns>
        public static IWebDriver GetLocalDriver(Browser _type)
        {
            return GetLocalDriver(_type, null);
        }

        /// <summary>
        /// Retrieves a local driver based on the Browser enumeration value and optional DesiredCapabilities
        /// </summary>
        /// <param name="_type">Browser enum </param>
        /// <param name="_capabilities">Capabilities for this Driver</param>
        /// <returns>WebDriver instance</returns>
        public static IWebDriver GetLocalDriver(Browser _type, DesiredCapabilities _capabilities)
        {
            if (sWebDriverRoot == null)
                throw new Exception("Failed to locate WebDrivers, WebDriver path is not set for DriverFactory.");

            switch (_type)
            {
                case Browser.FIREFOX:
                    return GetFirefoxDriver(_capabilities);
                case Browser.CHROME:
                    return GetChromeDriver(_capabilities);
                case Browser.IE:
                    return GetIEDriver(_capabilities);
                default:
                    break;
            }
            return null;
        }

        /// <summary>
        /// Create and return a Remote WebDriver based on the DesiredCapabilities given
        /// </summary>
        /// <param name="_caps">Capabilities, if any, for the remote webdriver</param>
        /// <param name="_connectionURL">URL for the remote webdriver to connect to</param>
        /// <returns>RemoteWebDriver instance</returns>
        public static IWebDriver GetRemoteDriver(DesiredCapabilities _caps, string _connectionURL)
        {
            return new RemoteWebDriver(new Uri(_connectionURL), _caps);
        }


        /// <summary>
        /// Create and return a Firefox driver based on the DesiredCapabilities given
        /// </summary>
        /// <param name="_caps">Capabilities for this Driver</param>
        /// <returns>Firefox Driver instance</returns>
        private static FirefoxDriver GetFirefoxDriver(DesiredCapabilities _caps)
        {
            FirefoxOptions ffopts = new FirefoxOptions();

            if (_caps != null)
            {
                //Do Firefox specific setup here
            }

            return new FirefoxDriver(sWebDriverRoot, ffopts);
        }


        /// <summary>
        /// Create and return a Chrome driver based on the DesiredCapabilities given
        /// </summary>
        /// <param name="_caps">Capabilities for this Driver</param>
        /// <returns>Chrome Driver instance</returns>
        private static ChromeDriver GetChromeDriver(DesiredCapabilities _caps)
        {
            ChromeOptions cropts = new ChromeOptions();

            if(_caps != null)
            {
                //Do Chrome specific setup here
            }

            return new ChromeDriver(sWebDriverRoot, cropts);
        }

        /// <summary>
        /// Create and return an Internet Explorer driver based on the DesiredCapabilities given
        /// </summary>
        /// <param name="_caps">Capabilities for this Driver</param>
        /// <returns>Internet Explorer Driver instance</returns>
        private static InternetExplorerDriver GetIEDriver(DesiredCapabilities _caps)
        {
            InternetExplorerOptions ieopts = new InternetExplorerOptions();

            if (_caps != null)
            {
                //Do IE specific setup here
            }
     
            return new InternetExplorerDriver(sWebDriverRoot, ieopts);
        }


    }
}
