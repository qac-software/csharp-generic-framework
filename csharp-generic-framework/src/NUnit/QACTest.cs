﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using csharp_generic_framework.Configuration;
using log4net.Config;
using log4net;

namespace csharp_generic_framework.NUnit
{
    class QACTest
    {
        public static Boolean runThroughIDE = true;

        [SetUp]
        public void Test()
        {
            Config.Load(new TOMLParser("~/../../../../resources/configuration/Config.toml"));
            Config Configuration = Config.GetInstance();
            GlobalContext.Properties["logDirectory"] = Configuration.Pathing.Logs;
            XmlConfigurator.Configure(new System.IO.FileInfo("log4net.xml"));
        }
    }
}
