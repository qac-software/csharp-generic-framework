﻿using log4net;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using csharp_generic_framework.Keywords;
using csharp_generic_framework.Keywords.Interaction;
using csharp_generic_framework.Keywords.Navigation;
using csharp_generic_framework.Keywords.Validation;

namespace csharp_generic_framework.POM
{
    /// <summary>
    /// Abstract Class representing all 'Page' objects in a Page Object Model (POM)
    /// 
    /// This abstract class takes care of providing helper functions to all Page objects as well as
    /// ensuring it is instantiated through Selenium's PageFactory for ease of Page writing.
    /// 
    /// </summary>
    /// <typeparam name="T">Allows method-chaining by modifiying the return type of the Page</typeparam>
    public abstract class Page<T> where T : Page<T>
    {
        protected ILog       mLogger;
        protected IWebDriver mWebDriver;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_driver">WebDriver instance</param>
        public Page(IWebDriver _driver)
        {
            // Set the internal instance of the WebDriver
            mWebDriver = _driver;
            // Configure the WebDriver to ensure that it is appropriately setup before testing 
            mWebDriver.Manage().Timeouts().ImplicitWait = System.TimeSpan.FromSeconds(10);
            // Initialize the page through Selenium's PageFactory to resolve annotated fields, if any
            PageFactory.InitElements(mWebDriver, this);
            // Instantiate a logger based on each derived classes type
            mLogger = LogManager.GetLogger(this.GetType().Name);
        }

        /// <returns>The title of this page</returns>
        public abstract string GetTitle();

        /// <returns>The URL of this page</returns>
        public abstract string GetURL();

        /// <returns>The internal WebDriver instance</returns>
        public IWebDriver GetDriver() { return mWebDriver; }

        /// <summary>
        /// Waits for the element to be enabled
        /// </summary>
        /// <param name="_element">WebElement object</param>
        /// <returns>Page object</returns>
        public T WaitForEnabled(IWebElement _element)
        {
            new Wait(mWebDriver).Execute(Wait.waitForEnabledStrategy(_element), null);
            return (T)this;
        }

        /// <summary>
        /// Waits for the element to be selected
        /// </summary>
        /// <param name="_element">WebElement object</param>
        /// <returns>Page object</returns>
        public T WaitForSelected(IWebElement _element)
        {
            new Wait(mWebDriver).Execute(Wait.waitForSelectedStrategy(_element), null);
            return (T)this;
        }

        /// <summary>
        /// Waits for the element to be visible
        /// </summary>
        /// <param name="_element">WebElement object</param>
        /// <returns>Page object</returns>
        public T WaitForVisible(IWebElement _element)
        {
            new Wait(mWebDriver).Execute(Wait.waitForVisibleStrategy(_element), null);
            return (T)this;
        }

        public T WaitForTime(int _time)
        {
            new Wait(mWebDriver).Execute(Wait.waitForTimeStrategy(_time), null);
            return (T)this;
        }

        /// <summary>
        /// Waits for the result of a custom predicate
        /// </summary>
        /// <param name="_func">Predicate to execute</param>
        /// <returns>Page object</returns>
        public T WaitFor(Func<IWebDriver, bool> _func)
        {
            new Wait(mWebDriver).Execute(_func, null);
            return (T) this;
        }

        /// <summary>
        /// Navigates to the URL returned by the GetURL() method of this page
        /// </summary>
        /// <returns>Page object</returns>
        public T NavigateTo()
        {
            if (!(new Navigate(Navigate.Destination.URL).Execute(mWebDriver, new string[] { GetURL() })))
                throw new Exception("Failed to navigate to the Page's default URL");
            return (T) this;
        }

        /// <summary>
        /// Navigates to the specified URL 
        /// </summary>
        /// <param name="_url">URL to navigate to</param>
        /// <returns>Page object</returns>
        public T NavigateTo(string _url) 
        {
            if (!(new Navigate(Navigate.Destination.URL).Execute(mWebDriver, new string[] { _url })))
                throw new Exception($"Failed to navigate to the given url: {_url}");
            return (T) this;
        }

        /// <summary>
        /// Navigates backwards
        /// </summary>
        /// <returns>Page object</returns>
        public T NavigateBack() 
        {
            if (!(new Navigate(Navigate.Destination.BACK).Execute(mWebDriver, null)))
                throw new Exception("Failed to navigate backwards");
            return (T) this;
        }

        /// <summary>
        /// Navigates forwards
        /// </summary>
        /// <returns>Page object</returns>
        public T NavigateForward()
        {
            if (!(new Navigate(Navigate.Destination.FORWARD).Execute(mWebDriver, null)))
                throw new Exception("Failed to navigate forwards");
            return (T) this;
        }

  
        /// <summary>
        /// Switch tabs to the specified tab
        /// 
        /// If a number is supplied, then a tab is chosen based on index from a list of available tabs in the current browser window
        /// If a string is supplied, then a tab is chosen based on the title of the tab itself
        /// </summary>
        /// <param name="_tab">Tab index or Tab name</param>
        /// <returns>Page object</returns>
        public T SwitchTab(string _tab)
        {
            if (!(new TabSwitch().Execute(mWebDriver, new string[] { _tab })))
                throw new Exception($"Failed to switch to tab with name/index: {_tab}");
            return (T) this;
        }

        /// <summary>
        /// Switch frames to the specified IFrame
        /// 
        /// If a number is supplied, then a frame is chosen based on index from all available frames on the page
        /// If a string is supplied, then a frame is chosen based on the id or name of the frame
        /// </summary>
        /// <param name="_frame">IFrame index or IFrame name/id</param>
        /// <returns>Page object</returns>
        public T SwitchFrame(string _frame)
        {
            if (!(new FrameSwitch().Execute(mWebDriver, new string[] { _frame })))
                throw new Exception($"Failed to switch to frame with name/id/index: {_frame}");
            return (T) this;
        }

        /// <summary>
        /// Inputs a string into a WebElement using Selenium's built-in SendKeys() functionality
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <param name="_input">Text to send to the WebElement</param>
        /// <returns>Page object</returns>
        public T InputText(IWebElement _element, string _input)
        {
            if (!(new Input().Execute(_element, new string[] { _input })))
                throw new Exception($"Failed to input text: {_input} into web-element");
            return (T) this;
        }

        /// <summary>
        /// Appends a string into a WebElement using Selenium's built-in SendKeys() functionality
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <param name="_input">Text to append to the WebElement</param>
        /// <returns>Page object</returns>
        public T AppendText(IWebElement _element, string _input)
        {
            if (!(new Input(true).Execute(_element, new string[] { _input })))
                throw new Exception($"Failed to append text: {_input} into web-element");
            return (T)this;
        }

        public T ClearText(IWebElement _element)
        {
            try
            {
                _element.Clear();
            }
            catch (Exception e)
            {
                throw new Exception("Failed to clear WebElement");
            }
            return (T) this;
        }

        /// <summary>
        /// Selects an option from the drop-down menu using the value
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <param name="_value">The value of the drop-down element to use as a locator</param>
        /// <returns>Page object</returns>
        public T SelectFromDropdownByValue(IWebElement _element, string _value) 
        {
            if (!(new Dropdown(Dropdown.SelectBy.VALUE).Execute(_element, new string[] { _value })))
                throw new Exception($"Failed to select from drop-down with value: {_value}");
            return (T) this;
        }

        /// <summary>
        /// Selects an option from the drop-down menu using the text
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <param name="_value">The text of the drop-down element to use as a locator</param>
        /// <returns>Page object</returns>
        public T SelectFromDropdownByText(IWebElement _element, string _value)
        {
            if (!(new Dropdown(Dropdown.SelectBy.VISIBLE_TEXT).Execute(_element, new string[] { _value })))
                throw new Exception($"Failed to select from drop-down with text: {_value}");
            return (T)this;
        }

        /// <summary>
        /// Selects an option from the drop-down menu using the index
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <param name="_value">The index of the drop-down element to use as a locator</param>
        /// <returns>Page object</returns>
        public T SelectFromDropdownByIndex(IWebElement _element, string _value)
        {
            if (!(new Dropdown(Dropdown.SelectBy.INDEX).Execute(_element, new string[] { _value })))
                throw new Exception($"Failed to select from drop-down with index: {_value}");
            return (T)this;
        }

        /// <summary>
        /// Clicks the given WebElement
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <returns>Page object</returns>
        public T ClickElement(IWebElement _element) 
        {
            if (!(new Click().Execute(_element, null)))
                throw new Exception("Failed to click web-element");
            return (T) this;
        }

        /// <summary>
        /// Validates the title of this page and ensures that the name is as we expect.
        /// Uses the concrete class' GetTitle() method to perform the check. 
        /// </summary>
        /// <returns>Page object</returns>
        public T ValidateTitle()
        {
            ValidateCurrentPage(mWebDriver, GetTitle());
            return (T) this;
        }


        /// <summary>
        /// Validates the title of this page and ensures that the name is as we expect
        /// </summary>
        /// <param name="_title">Title of the web-page</param>
        /// <returns>Page object</returns>
        public T ValidateTitle(string _title)
        {
            ValidateCurrentPage(mWebDriver, _title);
            return (T) this;
        }

        /// <summary>
        /// Validate the text of the given WebElement against the supplied text
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <param name="_text">Text to compare</param>
        /// <returns>Page object</returns>
        public T ValidateText(IWebElement _element, string _text) 
        {
            if (!(new ValidateElement().Execute(ValidateElement.GetTextEqualsStrategy(_text),_element)))
                throw new Exception($"Failed to validate the web-element text is: {_text}");
            return (T) this;
        }

        /// <summary>
        /// Validate that the text of the given WebElement contains the supplied text
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <param name="_text">Text to find</param>
        /// <returns>Page object</returns>
        public T ValidateTextContains(IWebElement _element, string _text)
        {
            if (!(new ValidateElement().Execute(ValidateElement.GetTextContainsStrategy(_text), _element)))
                throw new Exception($"Failed to validate the web-element contains the text: {_text}");
            else
                return (T) this;
        }

    

        /// <summary>
        /// Validate the visibility status of the given WebElement
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <returns>Page object</returns>
        public T ValidateVisible(IWebElement _element) 
        {
            if (!(new ValidateElement().Execute(ValidateElement.GetElementVisibleStrategy(true), _element)))
                throw new Exception("Failed to validate the visibility of the element");
            else
                return (T) this;
        }

        /// <summary>
        /// Validate the enabled status of the given WebElement
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <returns>Page object</returns>
        public T ValidateEnabled(IWebElement _element) 
        {
            if (!(new ValidateElement().Execute(ValidateElement.GetElementEnabledStrategy(true), _element)))
                throw new Exception("Failed to validate that the element is enabled - element is disabled");
            return (T) this;
        }

        /// <summary>
        /// Validate the enabled status of the given WebElement
        /// </summary>
        /// <param name="_element">WebElement to act on</param>
        /// <returns>Page object</returns>
        public T ValidateAttribute(IWebElement _element, String _attr, String _expected)
        {
            if (!(new ValidateElement().Execute(ValidateElement.GetAttributeEqualsStrategy(_attr, _expected), _element)))
                throw new Exception($"Failed to validate that the attribute: [{_attr}] against the expected text: [{_expected}].");
            return (T)this;
        }

        /// <summary>
        /// Validate against the WebElement using a custom predicate
        /// </summary>
        /// <param name="_pred">Predicate</param>
        /// <param name="_element">Element to act on</param>
        /// <returns></returns>
        public T ValidateElementWithPred(Predicate<IWebElement> _pred, IWebElement _element)
        {
            if (!(new ValidateElement().Execute(_pred, _element)))
                throw new Exception("Failed to validate the element with the given predicate");
            return (T)this;
        }

        /// <summary>
        /// Validate this Page type against a given Page type (as a string) (use obj.GetType().Name) 
        /// </summary>
        /// <param name="_typeName">The type name of the Page to check against</param>
        /// <returns>Page object</returns>
        public T ValidatePage(string _typeName)
        {
            if (!_typeName.Equals(this.GetType().Name))
                throw new Exception("Failed to validate page");
            return (T)this;
        }

        /// <summary>
        /// Validates the current state of the WebDriver object to ensure that we're on the correct page
        /// </summary>
        /// <param name="_driver">Concrete WebDriver object</param>
        /// <param name="_title">Page title</param>
        private void ValidateCurrentPage(IWebDriver _driver, string _title)
        {
            WebDriverWait _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            _wait.Until(d =>_title.Equals(d.Title));

            if (!_title.Equals(_driver.Title))
                throw new Exception("Failed to validate that this is the correct page.");
        }
    }
}
