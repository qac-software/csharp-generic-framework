﻿using csharp_generic_framework.Configuration;
using csharp_generic_framework.Misc;
using NUnitLite;
using NUnit.Engine;
using System.Xml;


using log4net;
using log4net.Config;

namespace csharp_generic_framework
{
    class Program
    {
        static Config Configuration;

        static void Main(string[] args)
        {
            /*
             * Parse arguments and perform necessary functionality defined in the CLI class
             * Implement your own CLI flags + functionality in the CLI.cs class
             * 
             * The configuration file is the only implemented value in the CLI using --config="filePath"
             */
            CLI _cli = new CLI();
            _cli.ParseArgs(args);

            /*
             * Load in an external configuration file using our custom TOML parser
             * Internally, this reads the TOML file and assigns the values to a predefined Java class.
             *
             * The Config class MUST be loaded with a parser prior to attempting to return an instance of Config.
             * An exception is thrown otherwise.
             */
            Config.Load(new TOMLParser(_cli.GetConfigFile()));
            Configuration = Config.GetInstance();

            /*
             * Ensure the DriverFactory contains the path to the WebDriver executables.
             * In our case, this is loaded from the external configuration 
             */
            DriverFactory.SetWebDriverRoot(Configuration.Pathing.WebDrivers);

            /*
             * Ensure the Log4Net XML configuration file is loaded and that the log 
             * output directory property is set to that defined in the external configuration
             */
            GlobalContext.Properties["logDirectory"] = Configuration.Pathing.Logs;
            XmlConfigurator.Configure(new System.IO.FileInfo("log4net.xml"));

            /*
             * Ensure that your Suite namespace contains a class containing the SetUpFixture annotation with
             * Parallelization scoped to the Fixtures.
             * 
             * Suites will run in SEQUENCE, with their Test Fixtures running in PARALLEL. 
             */ 
            new AutoRun().Execute(BuildNUnitArguments(new string[] {
                "csharp_generic_framework.Tests.Suites.ExampleSuite",
                "csharp_generic_framework.Tests.Suites.Suite2"
            })); 

            /*
             * UNCOMMENT BELOW TO RUN ALL UNIT TESTS IN ONE EXECUTION
             */
			// new AutoRun().Execute(BuildNUnitArguments(new string[] {
			// 	"csharp_generic_framework.src.UnitTests",
			// }));

            /* 
             * UNCOMMENT BELOW TO RUN INDIVIDUAL UNIT TESTS
             */
			// new AutoRun().Execute(BuildNUnitArguments(new string[] {
			// 	"csharp_generic_framework.src.UnitTests.TestConfig",
			// 	"csharp_generic_framework.src.UnitTests.TestDriverFactory",
			// 	"csharp_generic_framework.src.UnitTests.TestKeywords",
			// 	"csharp_generic_framework.src.UnitTests.TestPage",
			// }));
        }

        /// <summary>
        /// NUnit performs tests on the output binary, not the classes themselves.
        /// Therefore we have to build a list of arguments used to filter out the unwanted tests. 
        /// 
        /// Additionally, we set the result format to be of NUnit2 for common CI integration.
        /// Remove this if you would like NUnit3 results, or change the format to NUnit3
        /// </summary>
        /// <param name="_suites">Suite namespaces as an array </param>
        /// <returns></returns>
        private static string[] BuildNUnitArguments(string[] _suites)
        {
            string[] _toReturn = new string[] {
                "--where",
                "",
                "--result=TestResult.xml;format=nunit2"
            };

            foreach(string str in _suites)
            {
                if (_toReturn[1] != "")
                    _toReturn[1] += " || ";
                _toReturn[1] += $"namespace == {str}";
            }
            return _toReturn;
        }
    }
}
