﻿using OpenQA.Selenium;
using csharp_generic_framework.POM;

namespace csharp_generic_framework.Tests.Pages
{
    class LandingPage : Page<LandingPage>
    {
        public LandingPage(IWebDriver _driver) : base(_driver)
        {
        }

        public override string GetTitle()
        {
            return "My Account";
        }

        public override string GetURL()
        {
            return null;
        }
    }
}
