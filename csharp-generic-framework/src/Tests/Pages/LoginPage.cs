﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using csharp_generic_framework.POM;

namespace csharp_generic_framework.Tests.Pages
{
    class LoginPage : Page<LoginPage>
    {
        [FindsBy(How = How.Id, Using = "identifierId")]
        public IWebElement UsernameField { get; set; }
        [FindsBy(How = How.Id, Using = "identifierNext")]
        public IWebElement UsernameSubmit { get; set; }
        [FindsBy(How = How.Name, Using = "password")]
        public IWebElement PasswordField { get; set; }
        [FindsBy(How = How.Id, Using = "passwordNext")]
        public IWebElement PasswordSubmit { get; set; }

        public LoginPage(IWebDriver _driver) : base(_driver)
        {
        }

        public override string GetTitle()
        {
            return "Sign in - Google Accounts";
        }

        public override string GetURL()
        {
            return "https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin";
        }

 
        public LoginPage SetUsername(string _username)
        {
            InputText(UsernameField, _username);
            ClickElement(UsernameSubmit);
            WaitForTime(2);
            return this;
        }

 
        public LoginPage SetPassword(string _password)
        {
            InputText(PasswordField, _password);
            WaitForTime(2);
            return this;
        }


        public LandingPage Submit()
        {
            ClickElement(PasswordSubmit);
            return new LandingPage(GetDriver());
        }


        public LandingPage Login(string _username, string _password)
        {
            SetUsername(_username);
            SetPassword(_password);
            return Submit();
        }
    }
}
