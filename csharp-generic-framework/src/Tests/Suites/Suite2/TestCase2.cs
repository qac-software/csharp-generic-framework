﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using csharp_generic_framework.Tests.Pages;
using csharp_generic_framework.Misc;
using NUnit.Allure.Attributes;

namespace csharp_generic_framework.Tests.Suites.Suite2
{
    [TestFixture]
    [AllureFixture]
    class TestCase2
    {
        IWebDriver _driver;

        [SetUp]
        public void Before()
        {
            _driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
        }

        [TearDown]
        public void TearDown()
        {
            _driver.Close();
            _driver.Quit();
        }

        [Test]
        [AllureTest]
        public void SignInWithSubclassMethod()
        {
            (new LoginPage(_driver)).NavigateTo()
                .Login("qactest365@gmail.com", "qactest12");
        }


        [Test]
        [AllureTest]
        public void SignInWithChainedMethods()
        {
            LoginPage _page = new LoginPage(_driver);

            _page.NavigateTo()
                .InputText(_page.UsernameField, "qactest365@gmail.com")
                .ClickElement(_page.UsernameSubmit)
                .WaitForTime(3)
                .InputText(_page.PasswordField, "qactest12")
                .Submit()
                .ValidateTitle()
                .ValidatePage("LandingPage");
        }
    }


}
