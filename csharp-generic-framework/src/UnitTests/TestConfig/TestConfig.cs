﻿using csharp_generic_framework.Configuration;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_generic_framework.src.UnitTests.TestConfig
{
	[TestFixture]
	class TestConfig
	{
		// helper method
		private Config CreateConfig()
		{
			return new TOMLParser("~/../../../../resources/configuration/Config.toml").Parse();
		}

		[Test]
		public void TestTOMLParser()
		{
			Config _config = CreateConfig();
			Assert.IsNotNull(_config);
		}

		[Test]
		public void TestConfigPropertyAccess()
		{
			Config _config = CreateConfig();
			Assert.AreEqual(_config.Project.Name, "Project Name");
			Assert.AreEqual(_config.Pathing.Logs, "~/../../../../resources/logs/");
			Assert.AreEqual(_config.Capabilities.URL, "Test");
		}

		[Test]
		public void TestConfigFieldSet()
		{
			Config _config = CreateConfig();
			Assert.AreEqual(_config.Project.Name, "Project Name");

			_config.Project.Name = "NEW VALUE";
			Assert.AreEqual(_config.Project.Name, "NEW VALUE");
		}

	}
}
