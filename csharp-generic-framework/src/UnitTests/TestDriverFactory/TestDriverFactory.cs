﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using log4net;
using csharp_generic_framework.Misc;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace csharp_generic_framework.src.UnitTests.TestDriverFactory
{
	[SetUpFixture]
	class TestSetup
	{
		[OneTimeSetUp]
		public void RunBeforeAnyTests()
		{
			GlobalContext.Properties["webdriverDirectory"] = "./resources/webdrivers";
		}

		[OneTimeTearDown]
		public void RunAfterAnyTests()
		{ }
	}

	[TestFixture]
	class TestDriverFactory
	{
		IWebDriver _driver;

		[SetUp]
        public void Before()
        {
            _driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
        }

        [TearDown]
        public void TearDown()
        {
            _driver.Close();
            _driver.Quit();
        }

		[Test]
		public void createLocalChromeDriverFromEnum()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
			Assert.IsInstanceOf<ChromeDriver>(_driver);
		}

		[Test]
		public void createLocalFirefoxDriverFromEnum()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.FIREFOX);
			Assert.IsInstanceOf<FirefoxDriver>(_driver);
		}

		[Test]
		public void createLocalIEDriverFromEnum()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.IE);
			Assert.IsInstanceOf<InternetExplorerDriver>(_driver);
			_driver.Quit();
		}

		[Test]
		public void createRemoteWebDriver()
		{
			// needs a valid grid URL to verify
			_driver = DriverFactory.GetRemoteDriver(new DesiredCapabilities(), "https://www.google.com");
			Assert.IsInstanceOf<RemoteWebDriver>(_driver);
			_driver.Quit();
		}
	}
}
