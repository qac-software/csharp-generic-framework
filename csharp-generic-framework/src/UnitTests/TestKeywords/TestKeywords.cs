﻿using csharp_generic_framework.Misc;
using csharp_generic_framework.POM;
using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;

namespace csharp_generic_framework.src.UnitTests.TestKeywords
{
	class TempPage : Page<TempPage>
	{
		public TempPage(IWebDriver _driver) : base(_driver)
		{ }

		public override string GetTitle()
		{ return null; }

		public override string GetURL()
		{ return null; }
	}

	[SetUpFixture]
	class TestSetup
	{
		[OneTimeSetUp]
		public void RunBeforeAnyTests()
		{
			GlobalContext.Properties["webdriverDirectory"] = "./resources/webdrivers";
			GlobalContext.Properties["logDirectory"] = "./logs/tests";
		}

		[OneTimeTearDown]
		public void RunAfterAnyTests()
		{ }
	}

	class TestKeywords
	{
		IWebDriver _driver;

		[SetUp]
        public void Before()
        {
            _driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
        }

        [TearDown]
        public void TearDown()
        {
            _driver.Close();
            _driver.Quit();
        }
		[Test]
		public void TestFrameSwitchSuccess()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
			(new TempPage(_driver)).NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_button_test")
				.SwitchFrame("iframeResult");
		}

		// omitted due to dropped support for ExpectedActionAttribute in NUnit3+, and no
		// simple cross-language-version compatible workaround
		//@Test(expected = NoSuchFrameException.class)
		//   public void testSwitchFrameException() throws Exception {
		//       (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_button_test")
		//               .switchFrame("NotReal");
		//}

		[Test]
		public void TestClick()
		{
			(new TempPage(_driver)).NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_button_test")
				.SwitchFrame("iframeResult")
				.ClickElement(_driver.FindElement(By.XPath("/html/body/button")));

			try
			{
				_driver.SwitchTo().Alert().Accept();
			} catch (Exception e)
			{
				Assert.False(true);
			}
		}

		[Test]
		public void TestInput()
		{
			(new TempPage(_driver)).NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_form_submit")
				.SwitchFrame("iframeResult")
				.InputText(_driver.FindElement(By.Name("FirstName")), "Tyler")
				.InputText(_driver.FindElement(By.Name("LastName")),  "Remazki")
				.ClickElement(_driver.FindElement(By.XPath("/html/body/form/input[3]")))
				.WaitForVisible(_driver.FindElement(By.XPath("/html/body/div[1]")));
			Assert.IsTrue(_driver.FindElement(By.XPath("/html/body/div[1]"))
					.Text
					.Trim()
					.Contains("FirstName=MickeyTyler&LastName=MouseRemazki")
				);
		}

		[Test]
		public void TestTextContains()
		{
			(new TempPage(_driver)).NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_form_submit")
				.SwitchFrame("iframeResult")
				.InputText(_driver.FindElement(By.Name("FirstName")), "Tyler")
				.InputText(_driver.FindElement(By.Name("LastName")),  "Remazki")
				.ClickElement(_driver.FindElement(By.XPath("/html/body/form/input[3]")))
				.WaitForVisible(_driver.FindElement(By.XPath("/html/body/div[1]")))
				.ValidateTextContains(_driver.FindElement(By.XPath("/html/body/div[1]")), "MickeyTyler");
		}

		[Test]
		public void TestDropdown()
		{
			(new TempPage(_driver)).NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select")
                .SwitchFrame("iframeResult")
                .SelectFromDropdownByValue(_driver.FindElement(By.XPath("/html/body/select")), "opel");
			Assert.AreEqual(_driver.FindElement(By.XPath("/html/body/select")).GetAttribute("value"), "opel");
		}

		[Test]
		public void TestNavigation()
		{
			(new TempPage(_driver)).NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select")
                .NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_form_submit")
                .NavigateBack()
                .NavigateForward();
		}

		[Test]
		public void TestTabSwitch()
		{
			IWebDriver _dr;
			_dr = (new TempPage(_driver)).NavigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_link_target")
				.SwitchFrame("iframeResult")
				.ClickElement(_driver.FindElement(By.XPath("/html/body/a")))
				.SwitchTab("2")
				.ValidateTitle("W3Schools Online Web Tutorials")
				.SwitchTab("1")
				.ValidateTitle("Tryit Editor v3.5")
				.GetDriver();
			Assert.AreEqual(_dr.Url, "https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_link_target");
		}
	}
}
