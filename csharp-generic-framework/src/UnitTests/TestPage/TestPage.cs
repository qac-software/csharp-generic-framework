﻿using csharp_generic_framework.Misc;
using csharp_generic_framework.POM;
using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;

namespace csharp_generic_framework.src.UnitTests.TestPage
{
	class TempPage : Page<TempPage>
	{
		[FindsBy(How = How.Name, Using = "q")]
		public IWebElement searchbar { get; set; }

		public TempPage(IWebDriver _driver) : base(_driver)
		{ }

		public override string GetTitle()
		{ return "Google"; }

		public override string GetURL()
		{ return "https://www.google.ca"; }
	}

	[SetUpFixture]
	class TestSetup
	{
		[OneTimeSetUp]
		public void RunBeforeAnyTests()
		{
			GlobalContext.Properties["webdriverDirectory"] = "./resources/webdrivers";
			GlobalContext.Properties["logDirectory"] = "./logs/tests";
		}

		[OneTimeTearDown]
		public void RunAfterAnyTests()
		{ }
	}

	class TestPage
	{
		IWebDriver _driver;

		[SetUp]
        public void Before()
        {
            _driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
        }

        [TearDown]
        public void TearDown()
        {
            _driver.Close();
            _driver.Quit();
        }

		[Test]
		public void TestPageGetTitle()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
			TempPage pg = new TempPage(_driver);
			Assert.AreEqual(pg.GetTitle(), "Google");
		}

		[Test]
		public void TestPageGetURL()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
			TempPage pg = new TempPage(_driver);
			Assert.AreEqual(pg.GetURL(), "https://www.google.ca");
		}

		[Test]
		public void TestPageThroughSeleniumFactory()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
			TempPage pg = new TempPage(_driver);
			Assert.IsNotNull(pg.searchbar);
		}

		[Test]
		public void TestReturnSelfSuperclassMethods()
		{
			_driver = DriverFactory.GetLocalDriver(DriverFactory.Browser.CHROME);
			TempPage pg = new TempPage(_driver);
			pg.NavigateTo("http://images.google.ca");
			Assert.IsInstanceOf<TempPage>(pg.NavigateBack());
		}
	}
}
