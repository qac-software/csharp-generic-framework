﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_generic_framework.src.UnitTests
{
	// runs all unit tests
	[SetUpFixture]
	[Parallelizable(ParallelScope.Fixtures)]
	class UnitSuite
	{ }
}
